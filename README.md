# upflow test

## installing dependencies both client and server

    cd my-app / cd my-api
    yarn

## to start server

    cd my-api
    yarn start

## to start client

    cd my-app
    yarn start
