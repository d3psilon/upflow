import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      header: [],
      data: [],
      baseUrl: 'http://localhost:7777',
      order: 'asc',
      form: {
        firstname: '',
        lastname: '',
        age: ''
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.getRows();
  }

  getRows(params) {
    let baseUrl = this.state.baseUrl;
    if (params) {
      if (params.sort) {
        baseUrl += '?sort=' + params.sort;
      }
      if (params.order) {
        baseUrl += '&order=' + params.order;
      }
    }
    axios
      .get(baseUrl)
      .then(response => {
        this.setState({
          header: response.data.header,
          data: response.data.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  postRow() {
    let baseUrl = this.state.baseUrl;
    axios
      .post(baseUrl, this.state.form, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(response => {
        this.setState({
          header: response.data.header,
          data: response.data.data,
          form: {
            firstname: '',
            lastname: '',
            age: ''
          }
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleSort(key) {
    this.getRows({ sort: key, order: this.state.order });
    if (this.state.order === 'asc') {
      this.setState({ order: 'desc' });
    } else {
      this.setState({ order: 'asc' });
    }
  }

  deleteRow(id) {
    let baseUrl = this.state.baseUrl + '/' + id;
    axios
      .delete(baseUrl, this.state.form)
      .then(response => {
        this.setState({
          header: response.data.header,
          data: response.data.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const result = { ...this.state.form, [name]: value };

    this.setState({
      form: result
    });
  }

  handleSubmit(event) {
    this.postRow();
    event.preventDefault();
  }

  render() {
    let data_header = this.state.header.map((col, i) => {
      return (
        <td key={'col-' + i} onClick={() => this.handleSort(col.key)}>
          {col.label}
        </td>
      );
    });

    let data_rows = this.state.data.map((row, i) => {
      return (
        <tr key={'row' + i}>
          <td>{row.firstname}</td>
          <td>{row.lastname}</td>
          <td>{row.age}</td>
          <td>
            <button onClick={() => this.deleteRow(row.id)}>Supprimer</button>
          </td>
        </tr>
      );
    });

    return (
      <div>
        <table>
          <thead>
            <tr>
              {data_header}
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{data_rows}</tbody>
        </table>
        <form onSubmit={this.handleSubmit}>
          <label>
            First name:
            <input
              type="text"
              name="firstname"
              value={this.state.form.firstname}
              onChange={this.handleChange}
            />
          </label>
          <label>
            Last name:
            <input
              type="text"
              name="lastname"
              value={this.state.form.lastname}
              onChange={this.handleChange}
            />
          </label>
          <label>
            Age:
            <input
              type="number"
              name="age"
              value={this.state.form.age}
              onChange={this.handleChange}
            />
          </label>
          <input type="submit" value="Ajouter" />
        </form>
      </div>
    );
  }
}

export default App;
