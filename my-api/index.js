const express = require('express');
const app = express();
const uuidv1 = require('uuid/v1');

let json = require('./startdata');

// Allow CORS
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  next();
});

app.use(express.json());

// CREATE
app.post('/', function(req, res) {
  if (req.body) {
    if (req.body.firstname && req.body.lastname && req.body.age) {
      json.data.push({ id: uuidv1(), ...req.body });
      res.json(json);
    } else {
      res
        .status(400)
        .send('Please provide me at least firstname, lastname and age');
    }
  } else {
    res.status(400).send('Please provide me a body ;)');
  }
});

// READ
app.get('/', function(req, res) {
  let result = { ...json, data: [].concat(json.data) };
  if (req.query.sort) {
    result.data.sort(function(a, b) {
      var x = a[req.query.sort];
      var y = b[req.query.sort];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
  if (req.query.order === 'desc') {
    result.data.reverse();
  }
  res.json(result);
});

// UPDATE
app.put('/', function(req, res) {
  res.status(501).send('501 Not Implemented');
});

// DELETE
app.delete('/:rowid', function(req, res) {
  if (req.params.rowid) {
    const index = json.data.findIndex(x => x.id === req.params.rowid);
    if (index !== -1) {
      json.data.splice(index, 1);
      res.json(json);
    } else {
      res.status(400).send('Cannot find this row ID :/');
    }
  } else {
    res.status(400).send('Please provide me a row ID ;)');
  }
});

app.listen(7777, function() {
  console.log('Listening on port 7777');
});
